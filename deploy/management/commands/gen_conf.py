from django.core.management.base import NoArgsCommand
from django.template.loader import render_to_string as r2s
from django.conf import settings

def render(template,outname):
    s = r2s(template,{'settings':settings})
    p = settings.PROJ_ROOT/outname
    p.write_text(s)
    print outname
    print '-' * 30
    print s

def link(outname,linkname,destination):
    outpath = settings.PROJ_ROOT/outname
    return 'cd %s & ln -is %s %s' % (destination,outpath,outname)

class Command(NoArgsCommand):
    help = 'generate conf files'

    def handle_noargs(self,**options):

        s = ["#!/bin/bash"]

        template = 'nginx.conf'
        outname = template
        render(template,outname)
        cmd = link(outname,"%s.conf" % settings.PROJ_NAME,'/etc/nginx/sites-enabled')
        s.append(cmd)
        s.append('sudo service nginx reload')

        template = 'uwsgi.ini'
        outname = template
        render(template,outname)
        cmd = link(outname,"%s.ini" % settings.PROJ_NAME,'/etc/uwsgi/vassals')
        s.append(cmd)
        s.append('sudo service uwsgi reload')

        cmd = 'export WORKON_HOME=%s' % settings.WORKON_HOME
        s.append(cmd)

        cmd = 'mkvirtualenv %s' % settings.PROJ_NAME
        s.append(cmd)

        cmd = 'setvirtualenvproject %s %s' % (settings.ENV_ROOT,settings.PROJ_ROOT)
        s.append(cmd)

        sh = settings.PROJ_ROOT/'deploy.sh'
        sh.write_lines(s)
        sh.chmod(0755)
        print 'deloy.sh generated:'
        print sh.text()
        print 'run this script to deloy if you need(be careful!):'
        print './deploy.sh'
