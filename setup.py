from setuptools import setup, find_packages

setup(
    name="django-deploy",
    version='0.0.2',
    description="A reusable Django app for deploy",
    long_description=open("README.md").read(),
    author="guoqiao",
    author_email="guoqiao@gmail.com",
    license="MIT",
    keywords="django",
    url="https://bitbucket.org/nodedigital/django-deploy",
    packages=find_packages(),
    include_package_data=True,
)
