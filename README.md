django-deploy
=============

a django app helps to deploy:

* nginx
* uwsgi
* mysql
* phpmyadmin
* celery
* email
* supervisor
* virtualenv
* pip
* vim
...

required settings
================
* PROJ_ROOT: path to your project
* PROJ_NAME 
* ENV_ROOT: path to your virtualenv
* WWW_ROOT: path to your http server root, static, media, cache should be in it
* SERVER_NAME: example.com

required python package
================
* path.py

required system package
================

    apt-get install build-essential python-dev


usage
=====

add this to you requirements.txt:

    -e git+git@bitbucket.org:nodedigital/django-deploy.git#egg=django-deploy

install it:

    pip install -r requirements.txt

then, add `deploy` to INSTALLED_APPS.

When you run:

    ./manage.py render

this app will render the conf templates with your settings to conf/ dir.
You can then copy these the conf files to right location and change them.

If the default is just ok for you, you can run:

    sudo ./deploy.sh

this file will link the scripts to right path.
